import { Rectangle } from "./rectangle.js";
import { Square } from "./square.js";

var rectangle1 = new Rectangle(5, 10);
    var rectangle2 = new Rectangle(10, 20);

    console.log(rectangle1.getArea());
    console.log(rectangle2.getArea());

    var square1 = new Square(5);
    var square2 = new Square(10);

    console.log(square1.getArea());
    console.log(square2.getArea());

    console.log(square1.getPerimeter());
    console.log(square2.getPerimeter());

    console.log(rectangle1 instanceof Rectangle);
    console.log(rectangle1 instanceof Square);
    console.log(square1 instanceof Rectangle);
    console.log(square2 instanceof Square);