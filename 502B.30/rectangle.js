class Rectangle {
    _length;
    _width;

    constructor(paramLength, paramWidth) {
        this._length = paramLength;
        this._width = paramWidth;
    }

    getArea() {
        return this._length * this._width;
    }
}

export { Rectangle };