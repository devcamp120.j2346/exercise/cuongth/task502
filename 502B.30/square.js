import { Rectangle } from "./rectangle.js";

class Square extends Rectangle {
        
    constructor(paramLength) {
        super(paramLength, paramLength);
    }

    getPerimeter() {
        return 2 * (this._length + this._width);
    }
}

export { Square };