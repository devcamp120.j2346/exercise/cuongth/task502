import { Vehicle } from "./Vehicle.js";
import { Car } from "./Car.js";
import { Motorbike } from "./Motorbike.js";

var newVehicle1 = new Vehicle("BMW", 1912);
var newVehicle2 = new Vehicle("Madza", 1922);

console.log(newVehicle1);
console.log(newVehicle2);

console.log(newVehicle1.print());
console.log(newVehicle2.print());

var newCar1 = new Car("BMW", 1912, "12", "CX5");

console.log(newCar1);
console.log(newCar1.honk());

var newMotobike = new Motorbike("Yamaha", 1912, 12, CX5);

console.log(newMotobike);
console.log(newMotobike.honk());

console.log(newVehicle1 instanceof Vehicle);