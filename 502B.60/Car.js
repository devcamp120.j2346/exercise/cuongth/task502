import { Vehicle } from "./Vehicle.js";

class Car extends Vehicle {
    vId;
    modelName;

    constructor(paramId, paramModelName) {
        super(paramId, paramModelName);
    }

    honk() {
        return this;
    }
}

export { Car };
