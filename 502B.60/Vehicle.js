class Vehicle {
    brand;
    yearManufactured;

    constructor(paramBrand, paramYearManufactured) {
        this.brand = paramBrand;
        this.yearManufactured = paramYearManufactured;
    }

    print() {
        return this.brand;
    }
}

export { Vehicle };