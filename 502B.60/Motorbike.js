import { Vehicle } from "./Vehicle.js";

class Motorbike extends Vehicle {
    vId;
    modelName;

    constructor(paramId, paramModelName) {
        super(paramId, paramModelName);
    }

    honk() {
        return this;
    }
}

export { Motorbike };
